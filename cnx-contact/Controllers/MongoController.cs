using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using CCT.BLL;
using CCT.DAL.Entities;
namespace cnx_contact.Controllers
{
    public class MongoController : Controller
    {
        public static IConfigurationRoot Configuration { get; set; }
        public string Server = "mongodb://admin:secure@localhost:2277";
        public string database = "test";

        public async Task<JsonResult> getAllTeam()
        {
            var mongoDbService = new MongoDbService(database, "Team", Server);
            var allTeam = await mongoDbService.GetAllTeam();
            return Json(allTeam);
        }
        public async Task<JsonResult> getAllOffice()
        {
            var mongoDbService = new MongoDbService(database, "Office", Server);
            var allOffice = await mongoDbService.GetAllOffice();
            return Json(allOffice);
        }

        public async Task<JsonResult> getAllPerson()
        {
            var mongoDbService = new MongoDbService(database, "Person", Server);
            return Json(await mongoDbService.GetPerson());
        }
        public async Task<JsonResult> search(string str){
            var mongoDbService = new MongoDbService(database, "Person", Server);
            var result = await mongoDbService.Search(str);
            //var test = result[0].refTeam.ToString();
            return Json(result);           
        }

        // [HttpPostAttribute]
        // public async Task<JsonResult> updatePerson(Person p){
        //     var mongoDbService = new MongoDbService(database, "Person", Server);
        //     //await mongoDbService.updatePerson(p);
        //     return Json(p);     
        // }

        private bool checkDuplicateData(string[] inp){
            return inp.GroupBy(x => x).Any(g => g.Count() > 1) ? true : false;
        }
         
        private Message something(Person p){
            var msg = new Message();
            
            if (checkDuplicateData(p.contact.tel))    
                msg.message.Add("Contains duplicates tel1");  
            if(checkDuplicateData(p.contact.email))
                msg.message.Add("Contains duplicates email");
            if(checkDuplicateData(p.team))
                msg.message.Add("Contains duplicates team");
            if(checkDuplicateData(p.tags))
                msg.message.Add("Contains duplicates tags");                
            if(msg.message.Count()==0) 
                msg.error = false;
            return  msg;

        }
        [HttpPostAttribute]
        public async Task<JsonResult> updatePerson([FromBodyAttribute] Person p){
            var st = something(p); 
            Console.WriteLine(p.PersonId);       
            if(!st.error){
                 var mongoDbService = new MongoDbService(database, "Person", Server);
                 await mongoDbService.updatePerson(p);
            }
            return Json(st);
        }

        [HttpPostAttribute]
        public async Task<JsonResult> addPerson([FromBodyAttribute]Person p){
            var mongoDbService = new MongoDbService(database, "Person", Server);
            await mongoDbService.addPerson(p);
            return Json(true);
        }

        // [HttpPostAttribute]
        // public async Task<JsonResult> checkValue([FromBodyAttribute] Rev R){
        //     return Json(R);
        // }
    }
}
