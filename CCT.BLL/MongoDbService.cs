﻿using CCT.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace CCT.BLL
{
    public class MongoDbService
    {
        private IMongoCollection<Team> TeamCollection { get; }
        private IMongoCollection<Office> OfficeCollection { get; }
        private IMongoCollection<Person> PersonCollection {get ;}
        
        public MongoDbService(string databaseName,string collectionName,string databaseUrl)
        {
            var mongoClient = new MongoClient(databaseUrl);
            var mognoDatabase = mongoClient.GetDatabase(databaseName);
           
           TeamCollection = mognoDatabase.GetCollection<Team>(collectionName);
           OfficeCollection =  mognoDatabase.GetCollection<Office>(collectionName);
           PersonCollection = mognoDatabase.GetCollection<Person>(collectionName);
        }

        public async Task<List<Team>> GetAllTeam()
        {
            var team = new List<Team>();
            var allTeam = await TeamCollection.FindAsync(new BsonDocument());
            await allTeam.ForEachAsync(doc => team.Add(doc));
            return team;
        }

        public async Task<List<Office>> GetAllOffice(){
             var office = new List<Office>();
             var allOffice = await OfficeCollection.FindAsync(new BsonDocument());
             await allOffice.ForEachAsync(t => office.Add(t));
             return office;
        }

        public async Task<List<Person>> GetPerson(){
            var person = new List<Person>();
            var allPerson = await PersonCollection.FindAsync(new BsonDocument());
            await allPerson.ForEachAsync(p => person.Add(p));
            return person;
        }
        // public async Task<Person> Search(string str){
           
        //     //var x = new List<Person>();

        //      var filter = Builders<Person>.Filter.Regex(
        //             "firstName", new BsonRegularExpression(str, "i"));
        //      return await PersonCollection.Find(filter).FirstOrDefaultAsync();
        // }

         public async Task<List<Person>> Search(string str){
            var regex = new BsonRegularExpression(str, "i");
            var filter = Builders<Person>.Filter.Or(
                    Builders<Person>.Filter.Regex("firstName", regex),
                    Builders<Person>.Filter.Regex("lastName", regex),
                    Builders<Person>.Filter.Regex("nickName", regex),
                    Builders<Person>.Filter.Regex("contact.tel", regex),
                    Builders<Person>.Filter.Regex("contact.email", regex)
            );
            return await PersonCollection.Find(filter).ToListAsync();
        }

        public async Task addPerson(Person p){
            try{
                await PersonCollection.InsertOneAsync(p); 
            }
            catch{
                throw;
            }
            
        }
        public async Task updatePerson(Person p){
            // var filter = Builders<Person>.Filter.Eq("id",p.PersonId);
            // var update = Builders<Person>.Update
            //     .Set("contact",p.contact)
            //     .CurrentDate("lastModified");
            
            // await PersonCollection.UpdateOneAsync(filter, update);
            // var filter = Builders<Person>.Filter.Eq("_id",new ObjectId(p.PersonId));
            // var update = Builders<Person>.Update
            //     .Set("contact",p.contact)
            //     .Set("team",p.team)
            //     .p;            
            // await PersonCollection.UpdateOneAsync(filter, update);
            await PersonCollection.ReplaceOneAsync(
                doc => doc.PersonId == p.PersonId,p
            );
            
        }
        // public async Task Check(Person p){
        //     var filter = Builders<Person>.Filter.Eq("_id","58f872215eefdf0be8fa3160");
        //     var update = Builders<Person>.Update
        //         .Set("contact","update")
        //         .CurrentDate("lastModified");
            
        //     await PersonCollection.UpdateOneAsync(filter, update);
        // }
    }
}
