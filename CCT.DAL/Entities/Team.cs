using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace CCT.DAL.Entities
{
    public class Team
    {
        public string databaseName { get; set; }
        public string collectionName { get ; set; }
        public ObjectId id { get; set; }
    }
}