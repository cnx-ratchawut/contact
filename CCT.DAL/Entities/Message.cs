using System.Collections.Generic;

namespace CCT.DAL.Entities
{
    public class Message
    {
        public List<string> message { get; set; }
        public bool error { get; set; }

        public Message(){
            message = new List<string>();
            error = true;
        }
    }
}