using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.IdGenerators;

namespace CCT.DAL.Entities
{
    public class Person
    {
        //[BsonId(IdGenerator=typeof(GuidGenerator))]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        public string PersonId { get; set; }
        public string firstName { get; set; }
        public string lastName { get ; set; }
        public string nickName { get; set; }
        public Contact contact { get; set; } 
        public string[] team { get; set; } 
        public string office { get; set; }
        public string[] tags {get;set;}
        public MongoDBRef[]  refTeam { get; set; }
        //public ObjectId MyProperty { get; set; }
    }
}