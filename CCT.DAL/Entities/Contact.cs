using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace CCT.DAL.Entities
{
    public class Contact
    {
        //public List<tel> MyProperty { get; set; }
        public string[] tel { get; set; }
        public string[] email { get; set; }
    }
}